---
title: "Oufticoop"
date: 2020-07-29T11:55:26+02:00
draft: false
longText: true
categories: [Alimentation]
tags: [Coopérative, Circuit court, Commerce équitable, Maraîchage, Elevage, Produits laitiers, Boissons, Produits ménagers, Cosmétiques]
summary: >
    Ouvert en septembre 2019, Oufticoop est un magasin coopératif pour et par les citoyens, une réelle alternative à la grande distribution. Il permet à ses coopérateurs de trouver en un même lieu des produits alimentaires et non alimentaires issus de filières courtes, équitables, durables et socialement responsables.

youtubeId: 
valheureux: false
contact: 
    website: https://www.oufticoop.be/
    mail: 
    facebook: Oufticoop-2003796329891655
    twitter: 
    instagram: oufticoop_liege
locations:
  - name: Oufticoop
    address: Rue Curtius 10<br>4020 Liège
    hours: 
    phone: 
    googlePlaceId: ChIJR7JSFuXxwEcRyKH1zS6RxLw
    latitude: 
    longitude: 
---
Inspiré du modèle new-yorkais du *Park Slope Food Coop*, Oufticoop adopte un fonctionnement unique où chacun donne 3 heures mensuelles de son temps à la coopérative.  

Inclusif, Oufticoop entend soutenir les producteurs tout en permettant à toutes et tous de se fournir en produits de qualité à des prix accessibles.

Oufticoop, c’est enfin une aventure humaine, un laboratoire mobilisant l’intelligence collective au service d’un projet commun, créateur de lien social.
