---
title: "Au Fond du Jardin"
date: 2021-08-20T20:02:05+02:00
draft: false
longText: false
categories: [zero-dechets]
tags: [Vente directe,Vente en ligne,Créations textiles]
summary: "Des créations textiles ou upcycling."
youtubeId: 
valheureux: false
contact: 
    website: "https://aufonddujardin.be"
    mail: "contact@aufonddujardin.be"
    facebook: "aufonddujardinatelierboutique"
    twitter: 
    instagram: "aufonddujardinliege"
locations:
  - name: "Au fond du jardin"
    address: "Quai Marcellis 8<br>4020 Liège"
    hours: 
    phone: 
    googlePlaceId: "ChIJdf4-AQT6wEcRAVpey9ugnQ0"
    latitude: 
    longitude: 
---
Le zéro déchet Liège créé par une passionnée, des tissus remplis de bonne humeur et de qualité Oeko-tex. Parce qu’il est important d’être responsable de chacun de nos actes de consommation afin de limiter notre impact carbone. 
Vous pouvez acheter ces différents produits en ligne ou venir les découvrir dans le showroom d’*Au fond du Jardin*
