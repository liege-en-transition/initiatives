---
title: "La Coopérative Ardente"
date: 2020-04-29T09:44:54+02:00
categories: [Alimentation]
longText: true
tags: [Produits laitiers, Boissons, Farine, Boulangerie, Epices, Produits ménagers, Cosmétiques, Vrac, Vegan, Circuit court, Commerce équitable, Vente en ligne, Traiteur, Coopérative]
summary: "La Coopérative Ardente est une coopérative de consommateur·rice·s proposant plus de 800 produits alimentaires et ménagers issus de productions locales, bio et/ou équitables, au sein de son épicerie en ligne."
youtubeId:
contact:
    website: "https://www.lacooperativeardente.be"
    mail: "coop@lacooperativeardente.be"
    facebook: "LaCooperativeArdente"
    twitter: ""
    instagram: "la_cooperative_ardente"
locations:
  - name: "La coopérative ardente"
    address: "Rue aux Cailloux 110<br> 4420 Saint-Nicolas"
    hours: ""
    phone: "04 252 32 12"
    googlePlaceId: ChIJXdxbv9L7wEcRjyXvRZdutG8
---
Nous vous proposons, en étroite collaboration avec nos producteur∙rice∙s, de savoureux articles ultra-frais et issus de productions éthiques et à taille humaine.
Vivre et se nourrir sainement, à base de produits respectueux de l'humain & de l'environnement, est à la portée de toutes et tous.

Lancez-vous, avec nous!

# Comment ça marche ?

1 · Vous faites votre marché 24h/24 sur **www.lacooperativeardente.be**.

2 · Nous clôturons vos commandes le **dimanche minuit** maximum!

3 · Vous êtes livré·e·s le **mercredi** qui suit, en point de retrait, ou le **jeudi** qui suit, à domicile (en région liégeoise) ! 
