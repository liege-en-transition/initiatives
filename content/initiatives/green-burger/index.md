---
title: "Green Burger"
date: 2021-08-20T18:16:49+02:00
draft: false
longText: false
categories: ["zz-autres"]
tags: [Végan,Circuit-court,Restauration,Monnaie locale,Solidarité,Sensibilisation,Education]
summary: "Greenburger se positionne comme le spécialiste du burger vegan sur Liège, et se base pour se faire sur une économie circulaire."
youtubeId: 
valheureux: true
contact: 
    website: "http://greenburger.be"
    mail: "hello@greenburger.be"
    facebook: "greenburger.liege"
    twitter: 
    instagram: "greenburger.liege"
locations:
  - name: "Green Burger"
    address: "Rue du pont 13<br>4000 Liège"
    hours: 
    phone: 
    googlePlaceId: ChIJteuXCAz6wEcRMDEByKaxf40 
    latitude: 
    longitude: 
---
PLUS QU'UN BURGER,
UN CONCENTRÉ DE VALEURS !

*Vegan, végétalien?*
Le végétalisme est un régime alimentaire qui exclut tous produits d’origine animale et issus de l’exploitation animale. Toute notre carte, du burger au dessert, est donc 100% végétale!
