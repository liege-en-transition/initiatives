---
title: "Les Potikès de Grannie"
date: 2021-08-20T19:31:04+02:00
draft: false
longText: false
categories: ["zz-autres"]
tags: [Boissons,Restauration,Traiteur,Evénèment]
summary: "Potiqueterie et sandwicherie de spécialités liégeoises"
youtubeId: 
valheureux: false
contact: 
    website: 
    mail: "lespotikes@gmail.com"
    facebook: "lespotikesdegrannie"
    twitter: 
    instagram: "Lespotikes_liege"
locations:
  - name: "Les Potikès de Grannie"
    address: "En Neuvice 53<br>4000 Liège"
    hours: 
    phone: 
    googlePlaceId: ChIJx4VHQfj7wEcResrbySKqtA4
    latitude: 
    longitude: 
---
“Manger local ou bio (mais toujours liégeois!) en Neuvice, c’est désormais possible aux “Potikès de Grannie”. Derrière les fourneaux, une grand-mère bien active, prof pensionnée mais bien passionnée de cuisine” 
La Meuse, 16/04/2019
