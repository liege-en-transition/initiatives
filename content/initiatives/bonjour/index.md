---
title: "Bonjour"
date: 2020-04-29T11:25:35+02:00
categories: [zero-dechets]
tags: [Seconde main,Sensibilisation,Education,Ateliers]
summary: "Magasin de seconde main pour enfants comprenant vêtements, matériel de puériculture, jouets,...
mais également pourvu d’une belle pièce à l’arrière du magasin permettant un espace d'événements et ateliers (yoga, méditation, ateliers de portage bébé, ateliers de réorientation professionnelle, etc ...)"
youtubeId: ""
contact:
    website: ""
    mail: "bonjour.laveu@gmail.com"
    facebook: "Bonjour-797238977310202"
    twitter: ""
    instagram: ""
locations:
  - name: 
    address: "Rue du Laveu 30<br>4000 Liège"
    hours:
    phone:
    latitude: 50.632087
    longitude: 5.556761
    googlePlaceId: "ChIJdY2XmtH7wEcRwksR4uMR9BA"
---
