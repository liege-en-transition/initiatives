---
title: "Repair Cafe"
date: 2020-02-25T23:45:22+01:00
categories: ["zero-dechets"]
tags: ["Solidarité,Evènement,Réparation"]
summary: >
    Que faire d’une chaise au pied branlant? D’un grille pain qui ne marche plus? D’un pull troué ou d’un ordinateur en panne? Les jeter? Pas question!
    On vous aide à les remettre sur pied au Repair Café.
youtubeId: ""
contact: 
    website: "https://www.repairtogether.be"
    mail: ""
    facebook: ""
    twitter: ""
    instagram: ""
#locations:
#  - name: ""
#    address: ""
#    hours: ""
#    phone: ""
#    coordinates: []
---