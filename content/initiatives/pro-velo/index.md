---
title: "Pro Velo"
date: 2020-02-25T22:21:51+01:00
categories: ["zz-autres"]
longText: true
tags: [Réparation,Evènement,Services,Mobilité douce]
summary: >
    Pro Velo Liège, situé au point vélo de la gare de Liège-Guillemins, propose de nombreux services vélo comme 
    la réparation, la location, la gravure, la formation, la vente de vélos et d'accessoires, des visites guidées ou encore 
    des afterworks thématiques.
youtubeId: ""
contact: 
    website: "https://www.provelo.org/fr/implantation/liege"
    mail: "liege@provelo.org"
    facebook: "proveloliege"
    twitter: ""
    instagram: ""
locations:
  - name: "Pro Vélo"
    address: "Place des Guillemins 2"
    hours: ""
    phone: "04 222 99 54"
    latitude: 
    longitude: 
    googlePlaceId: ChIJodw2FPf5wEcR8dW9IZ04PHQ
---
L'objectif est de soutenir les cyclistes actuels et potentiels, enseigner la conduite à vélo, diffuser une image positive 
du vélo et accompagner la politique cycliste. Pro Velo contribue ainsi à une meilleure qualité de vie !
