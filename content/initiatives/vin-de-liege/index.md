---
title: "Vin de Liege"
date: 2020-07-09T13:06:47+02:00
draft: false
longText: true
categories: [a_producteur]
tags: [Vente directe,Circuit court,Boissons,Coopérative]
summary: >
    L’idée de créer une activité viticole en région liégeoise a vu le jour au départ des activités de l’ASBL La Bourrache, une entreprise de formation par le travail qui exploite des terrains sur les hauteurs de Liège. 
youtubeId: 
valheureux: false
contact: 
    website: https://www.vindeliege.be
    mail: info@vindeliege.be
    facebook: VindeLiege
    twitter: 
    instagram: 
locations:
  - name: Vin de Liège
    address: Rue Fragnay 64<br> 4682 Oupeye
    hours: 
    phone: 04 344 00 14
    googlePlaceId: ChIJ64GTtOvvwEcRh2pCf8gWe9A
    latitude: 
    longitude: 
---
Les coteaux de la Citadelle, ainsi que les terrains situés dans la vallée de la Meuse, ont été historiquement consacrés à la production de vin.

Une fois lancé, le projet a rapidement dépassé le cadre de cette ASBL pour devenir un réel projet économique d’envergure. Les initiateurs du projet se sont alors entourés de conseillers (viticulteurs, pépiniéristes et consultants, belges, allemands et français) en vue de créer un domaine viticole sur Liège.

Entamée en **septembre 2009**, une étude de faisabilité approfondie s’est successivement penchée sur :

· la faisabilité technique d’une production de vin de qualité en Belgique

· l’existence d’un marché pour écouler les produits belges,

· la disponibilité de terrains adéquats pour la production de vin

· la faisabilité économique du projet qui fait l’objet d’un plan d’affaires complet,

· la possibilité de produire un vin issu de l’agriculture biologique.


Sur base de cette expertise, le projet a été jugé réalisable et économiquement rentable. Les statuts de la société ont alors été rédigés et un premier appel à coopérateurs a permis de rassembler un capital initial. La coopérative à finalité sociale Vin de Liège a été créée le 21 décembre 2010.

**En 2011 et 2012**, les membres actifs de Vin de Liège ont déployé leur énergie autour de deux axes :

· La définition des choix techniques du vignoble, des vins et du chai

· La promotion de Vin de Liège afin d’accroître sa notoriété et de convaincre de nouveaux coopérateurs privés et institutionnels


Les premières vendanges ont été réalisées à l’automne 2014. Elles ont permis l’élaboration de plus de 25 000 bouteilles qui ont été principalement commercialisées à partir du 23 mai 2014 lors de l’inauguration du chai. À terme, Vin de Liège compte produire **plus de 100 000 bouteilles par an**.


