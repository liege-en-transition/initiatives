---
title: "RAYON9"
date: 2020-07-08T10:05:11+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Service,Mobilité douce,Coopérative,Solidarité]
summary: >
    RAYON9 est une entreprise liégeoise qui propose aux entreprises et aux commerçants un service de transport écologique, rapide et efficace: la livraison cyclo-urbaine.
youtubeId: 
valheureux: false
contact: 
    website: https://www.rayon9.be/
    mail: 
    facebook: rayon9coop
    twitter: Rayon9coop
    instagram: rayon9coop
locations:
  - name: 
    address: 
    hours: 
    phone: 
    googlePlaceId: 
    latitude: 
    longitude: 
---
Chez RAYON9, les coursiers sont salariés et les livraisons sont assurées exclusivement à deux roues: un vélo de course pour les colis légers, des vélos-cargos à assistance électrique pour un chargement jusqu’à 230 kg. 

RAYON9 a été fondée en 2016 par 3 amis liégeois concernés par les problèmes de mobilité, d’écologie et d’emploi dans leur ville. 

Constituée sous la forme d’une société coopérative agréée comme entreprise sociale, elle est aujourd’hui propulsée par 120 coopérateurs et coopératrices. Leur objectif: rendre la ville plus verte et promouvoir l’emploi des jeunes!.
