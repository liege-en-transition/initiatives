---
title: "Habiter au naturel"
date: 2020-07-08T10:09:52+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Sensibilisation,Education,Ateliers]
summary: >
    Atelier d'architecture d'intérieur écologique situé à Angleur. 
youtubeId: 
valheureux: false
contact: 
    website: https://habiteraunaturel.be
    mail: pauline@habiteraunaturel.be
    facebook: Habiteraunaturel
    twitter: 
    instagram: habiter_au_naturel
#    locations:
#    - name: 
#        address: 
#        hours: 
#        phone: 
#        googlePlaceId: 
#        latitude: 
#        longitude: 
---
# Conseils et accompagnement en aménagement éco-responsable et en rénovation intérieure #

Avec Habiter au naturel, Pauline Fusini exerce le métier d’architecte d’intérieur avec tout le sens qu'elle lui donne; elle aide ses clients à vivre dans une habitation cohérente avec leurs valeurs, en utilisant raisonnablement les ressources existantes, dans un souci constant de collaboration harmonieuse avec ce et ceux qui nous entourent. 

Cela se traduit par l’emploi de matériaux naturels ou d’objets récupérés; par des collaborations avec des artisans passionnés et par une attention particulière portée à l’histoire du bâtiment et au mode de vie de leurs habitants. Son aide se décline selon les besoins et les envies : simple conseil ou visite inspirante, création d’un projet d’aménagement global ou recherche de meubles chinés ; Habiter au naturel, c'est aussi sortir des sentiers battus et explorer d'autres façons de faire ensemble. 

# Workshops autour de la fabrication de peintures naturelles #
Pauline vous ouvre par ailleurs les portes de sa maison éco-rénovée une fois par moins pour un atelier en petit groupe autour de la fabrication de peintures naturelles. Elle offre l’espace, le temps et les outils pour se réapproprier des recettes saines et durables et les reproduire ensuite chez soi.
