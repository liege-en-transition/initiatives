---
title: "Ecotopia"
date: 2020-07-08T10:11:04+02:00
draft: false
longText: true
categories: [a_producteur]
tags: [Circuit court,Maraîchage,Education,Artisanat,Ateliers,Pépinière,Vente directe,Solidarité,Evènement]
summary: >
    Ancienne pépinière de 5ha, louée et restaurée par l'asbl, a été reconvertie en lieu de culture (de l'esprit et de la terre) et s'insère dans [la Ceinture Alimentaire Liégeoise](http://catl.be) 
youtubeId: 
valheureux: false
contact: 
    website: http://www.ecotopiatilff.org
    mail:
    facebook: ecotopiatilff
    twitter: 
    instagram: 
locations:
  - name: ECOTOPIA Tilff
    address: Rue d'Angleur 92<br> 4130 Esneux
    hours: 
    phone: 
    googlePlaceId: ChIJRbpvcr33wEcRxjRl54F1u0E
    latitude: 
    longitude: 
---
Les activités d'ECOTOPIA misent sur un futur qui serait respectueux de la nature, raisonnable en matière de consommation d'énergies fossiles, valorisant pour l'homme tout en restreignant le côté matériel.

L'agro-écologie (permaculture), la redécouverte des savoirs anciens, la promotion des arts et de l'artisanat, des rencontres forums et conférences, l'organisation de stages, de formations pour adultes, la réinsertion sociale.

ECOTOPIA fonctionne sans contrainte de convictions politiques, philosophiques ou religieuses

