---
title: "Go Vegan Shop"
date: 2020-04-29T09:29:38+02:00
categories: ["Alimentation"]
tags: [Circuit court,Vrac,Vegan,Vêtements,Produits laitiers]
summary: >
    Goveg Vegan Shop est un magasin regroupant des articles alimentaires, vestimentaires et cosmétiques produits avec le plus grand respect du monde animal.
youtubeId: ""
contact: 
    website: "https://govegveganshop.weebly.com/"
    mail: "goveg.be@gmail.com"
    facebook: "Goveg-vegan-shop-794773080606849"
    twitter: ""
    instagram: ""
locations:
  - name: ""
    address: "Rue Hors-Château 8, 4000 Liège"
    hours: ""
    phone: "04 222 09 21"
    latitude: 50.646555
    longitude: 5.576713
    googlePlaceId: "ChIJKSUebwz6wEcRmR7LOdBRHNQ"
---

Situé dans le quartier Hors-Château, la majorité des produits sont certifiés bio et, à part quelques exceptions, sans huile de palme.
Les accessoires disponibles chez Goveg sont bien sûr respectueux des animaux mais aussi des humains, ils travaillent exclusivement avec des marques produisant en Europe ou labellisées commerce équitable
Goveg c’est aussi une ligne de t-shirts.
