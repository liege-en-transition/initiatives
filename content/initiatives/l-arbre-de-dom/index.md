---
title: "L'Arbre de Dom"
date: 2020-07-08T10:09:36+02:00
draft: false
longText: false
categories: [zero-dechets]
tags: [Circuit court,Vrac,Epices,Farines,Maraîchage]
summary: >
    L’arbre de Dom est une épicerie bio zéro déchet ambulante.
    Vous y trouverez des fruits secs, des céréales, des graines, des légumineuses, des épices, des riz & pâtes, des farines & sucres, des granolas ... et les ingrédients de base pour fabriquer vos produits d’hygiène et d’entretien.
youtubeId: 
valheureux: false
contact: 
    website: www.larbrededom.be
    mail: info@larbrededom.be
    facebook: Larbre-de-Dom-100610461386879
    twitter: 
    instagram: 
locations:
  - name: L'Arbre de Dom
    address: Rue de Fechery 1<br> 4140 Sprimont
    hours: 
    phone: 04/95128046
    googlePlaceId: ChIJjZVuHTdfwEcRxZf6fXbsEMA
    latitude: 
    longitude: 
---
