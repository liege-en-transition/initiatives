---
title: "Justin Mange Bien"
date: 2020-04-29T09:29:29+02:00
categories: ["Alimentation"]
tags: [Circuit court,Produits laitiers,Produits ménagers,Cosmétiques,Elevage,Maraîchage,Boissons,Vrac,Traiteur]
summary: Du goût, du local, et du bio...pour des produits "bons & sains" dans nos assiettes! Magasin d’alimentation bio et locale avec plats sains à emporter.
youtubeId:
contact: 
    website: "https://jmb-online.be"
    mail: hello@jmb.bio
    facebook: jmb.bio
    twitter: ""
    instagram: ""
locations:
  - name: ""
    address: "Rue les Oies 1<br> 4052 Chaudfontaine"
    hours: ""
    phone: "0494 52 33 26"
    latitude: 50.569205
    longitude: 5.620994
    googlePlaceId: "ChIJH4BaQ0v2wEcRMpkIGDZRMR4"
---
