---
title: "Slow in Liege"
date: 2020-07-08T10:09:25+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Blog,Sensibilisation]
summary: >
    Slow In Liège est un blog liégeois désireux de s’inscrire dans la mouvance « slow » (slow food, slow cosmétique, slow life,…) en mettant en relation des produits éthiques et des alternatives durables avec des boutiques locales.
youtubeId: 
valheureux: false
contact: 
    website: https://slowinliege.be/
    mail: info@slowinliege.be
    facebook: slowinliege
    twitter: 
    instagram: slowinliege
locations:
  - name: Liège
    address: 
    hours: 
    phone: 
    googlePlaceId: 
    latitude: 
    longitude: 
---
La ville de Liège connaît – depuis quelques années déjà – un véritable essor au niveau de son mode de consommation (**éthique, écologique, locale et/ou durable**). De plus en plus d’enseignes voient le jour avec pour objectif de proposer à la vente des produits issus d’une agriculture biologique, en collaboration avec des producteurs locaux. Le **zéro déchet** ne peut désormais plus être considéré comme une *mode*. À Liège, elle est devenue un véritable état d’esprit ! 

C’est forte de cette conviction que le blog **Slow in Liège** a vu le jour. Le concept : faire le lien entre la problématique et son alternative, puis entre la solution et les moyens locaux de la concrétiser.

Le site n'a plus été mis à jour depuis juin 2020 mais il reste intéressant à bien des égards. 
