---
title: "Demain, L'Epicerie Zero Dechet"
date: 2020-04-29T09:30:09+02:00
categories: ["zero-dechets"]
tags: [Produits laitiers,Boissons,Farine,Boulangerie,Epices,Produits ménagers,Cosmétiques,Vrac,Circuit court,Commerce équitable,En ligne,Monnaie locale,Solidarité,Sensibilisation,Education,Ateliers]
summary: "Une épicerie à Spa où vous trouverez des produits secs (pâtes, riz, céréales, bonbons, épices, thés, cafés, légumineuses, huiles, vinaigres, …), des fruits et légumes locaux et de saison uniquement, des articles pour la maison, des articles pour l’hygiène, et le tout NON EMBALLE."
youtubeId: OW4V7Jvg-1M
longText: true
contact: 
    website: "https://demain-lepicerie.be"
    mail: "cath@demain-lepicerie.be"
    facebook: "demain.lepicerie"
    twitter: ""
    instagram: ""
locations:
  - name: ""
    address: Avenue Reine Astrid 7<br> 4900 Spa
    hours: ""
    phone: "087 70 56 12"
    latitude: 50.492945
    longitude: 5.862447
    googlePlaceId: "ChIJDVBgtNRjwEcR6xcksnSmWzs"
---
Le but : on essaie autant que possible de réduire le plastique jetable ainsi que l’empreinte carbone de nos aliments. On axe donc sur le local et les produits de producteurs et non de grande distribution. On prend soin de la planète, mais aussi de l’homme en favorisant le fair-trade.
