---
title: "Vtopia"
date: 2020-08-12T11:14:00+02:00
draft: false
longText: false
categories: [a_producteur]
tags: [Maraîchage,Vegan,Auto cueillette,Circuit court]
summary: >
    Vtopia est un projet d'agriculture urbaine, participative, végétalienne situé à Oupeye - Vivegnis. 
youtubeId: 
valheureux: false
contact: 
    website: http://www.vtopia.be/
    mail: 	jdm@jardinierdumonde.be
    facebook: delaterrealassiette.be
    twitter: 
    instagram: 
locations:
  - name: VTopia
    address: Rue Fût-Voie 37<br>4683 Oupeye
    hours: 
    phone: 
    googlePlaceId: ChIJE7jVthvxwEcR-Bi-512jlds
 
---
Vtopia s'adresse principalement aux Véganes, végétariens et plus largement aux mangeurs soucieux de réduire l'impact environnemental de leur alimentation. Vtopia est un lieu participatif d’expérimentation d’un nouveau rapport à notre environnement, un grand jardin à cultiver ensemble en coopération avec le vivant.

Depuis juin 2020 nous cultivons ce site de 2,6 ha, afin d'y mener l'agriculture la plus nourricière et la plus respectueuse de l'environnement possible : une production végétale très diversifiée, aucun lien avec l'exploitation animale, agroforesterie, création de zones refuges pour la biodiversité, fertilisation organique intensive du sol vivant,…

Tout le monde y est le bienvenu, il est possible de participer à la production en s’initiant aux savoirs de la terre, lors de chantiers participatifs hebdomadaires. Depuis septembre 2020, vous pouvez également y venir récolter vos légumes vous-même selon le principe de l'auto-cueillette, dans un cadre idyllique.
