---
title: "Les 3 R"
date: 2020-08-21T14:37:57+02:00
draft: false
longText: true
categories: [zero-dechets]
tags: [Seconde-main,Réparation]
summary: >
    En 1982, le Téléservice de Plombières crée un Service Mobilier de seconde main dans la caserne de Hombourg, dans le but de générer des fonds pour soutenir ses activités sociales. En 1993, les locaux du Service Mobilier sont devenus trop petits et l’asbl “De Bouche à Oreille”, dont le Service Mobilier fait partie, achète un bâtiment au 218 de la rue Mitoyenne à Herbesthal, inauguré en février 1994 sous le nom actuel “Les 3R”.
youtubeId: 
valheureux: false
contact: 
    website: https://www.les3r.be/
    mail: info@les3r.be
    facebook: dbao.3R
    twitter: 
    instagram: 
locations:
  - name: Les 3 R
    address: Rue Mitoyenne 218<br>4710 Lontzen
    hours: 
    phone: 087 89 08 39
    googlePlaceId: ChIJH7W47K6PwEcRicPwrGvzcok
    latitude: 
    longitude: 
---
D’autre part, en 1986, un magasin de vêtements de seconde main est mis sur pieds par “La Potée Ose”, le Téléservice de Plombières et le Téléservice de Welkenraedt. C’est la naissance du magasin “La Goutte d’Eau”, au sein de l’asbl “De Bouche à Oreille”.

Fin septembre 2013, l’asbl « De Bouche à Oreille » décide de transformer “La Goutte d’Eau” en une Boutique alternative citoyenne qui prend le nom de “Caract’R” qui vise alors à rassembler, dans un seul et même lieu, des biens et des services pour consommer de manière plus durable.

En 2017, Les 3R déménagent pour une période de transformation de leurs bâtiments, qui dure près de 2 ans. Pendant ce temps, Caract’R et Les 3R vivent côte à côte, au 220 de la rue Mitoyenne à Herbesthal, et fusionnent en une seule enseigne: “Les 3R”.

Le 30 novembre 2019, l’ensemble des activités des 3R réintègre les bâtiments du 218 de la rue Mitoyenne, complètement transformés.
