---
title: "La Reid Du Temps"
date: 2020-07-08T10:10:41+02:00
draft: false
longText: true
categories: [a_producteur]
tags: [Maraîchage,Education,Ateliers,Vente directe]
summary: >
    La Reid du temps est un nouveau projet de maraîchage débuté en 2020 à la Reid. Derrière ce maraichage, se trouvent Ludovic Bollette, sa famille et ses amis. 
youtubeId: 
valheureux: false
contact: 
    website: 
    mail: lareiddutemps@gmail.com
    facebook: La-Reid-du-temps-104739177936969
    twitter: 
    instagram: 
locations:
  - name: La Reid du Temps
    address: Route du Fraineux <br> 4910 La Reid
    hours: 
    phone: 
    googlePlaceId: ChIJSfHFfcBhwEcRoR-lNehRyFo
    latitude: 
    longitude: 
---
Après la création de la plateforme **“Mangez Local”** et la réalisation du documentaire “Tandem local” mettant en valeur nos producteurs belges, Ludovic Bollette est passé de l’observation et la promotion à la production. Les élèves de l’école maternelle de Poleur ont déjà pu venir découvrir l’élaboration de ce beau projet de maraîchage.

Vente de paniers découvertes ainsi que de légumes en vrac venant de la *Reid du temps* mais aussi de *Joël Ruth* et du *jardin du Ratouchamps*. 

Venez découvrir ce nouveau projet plein d’enthousiasme.
