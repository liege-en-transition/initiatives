---
title: "Le Potiquet"
date: 2020-07-08T10:09:19+02:00
draft: false
longText: false
categories: [Alimentation]
tags: [Circuit court,Boulangerie,Maraîchage,Boissons,Epices,Cosmétiques,Vrac]
summary: >
    Dans cette épicerie d'Outremeuse, on privilégie les produits locaux, le bio et le vrac. Vous y trouverez de l'alimentaire (pains, fruits, légumes, chocolats, céréales, épices, boissons,...), des produits de soin du corps mais également pour l'entretien de la maison. Le mot “Potiquet” signifie “petit récipient”. Il trouve son origine en Belgique car il se calque sur le diminutif flamand "potteke" qui signifie “petit pot”, ce qui lui donne une couleur locale. Ce sobriquet est donc parfaitement associé à leur démarche ainsi qu’à leur commerce puisque leurs produits y sont proposés en vrac. Chacun n’achètera ainsi que la quantité qui lui sera nécessaire et se passera d’emballages superflus.

youtubeId: 
valheureux: true
contact: 
    website: http://www.lepotiquet.be
    mail: bonjour@lepotiquet.be
    facebook: Le-Potiquet-369251146989813
    twitter: 
    instagram: 
locations:
  - name: Le Potiquet
    address: Rue Puits-en-Sock 149<br> 4020 Liège
    hours: 
    phone: 
    googlePlaceId: ChIJZcA2I93xwEcRdNWPrat024E
    latitude: 
    longitude: 
---
