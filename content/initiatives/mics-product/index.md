---
title: "Mic's Products"
date: 2020-07-30T10:10:21+02:00
draft: false
longText: true
categories: [a_producteur]
tags: [Boissons,Circuit court]
summary: >
    Issu d’une longue tradition d’artisans amoureux de matières nobles, Michel Bouillon est l’arrière petit-fils de Louis Bouillon qui créa sa distillerie du côté de Grâce-Hollogne en 1898. 
youtubeId: 
valheureux: false
contact: 
    website: http://www.micsproducts.be/
    mail: michel.bouillon@gmail.com
    facebook: Mics-Products-731260220285039
    twitter: 
    instagram: 
locations:
  - name: Mic's Products
    address: Grand Route 53<br>4537 Verlaine
    hours: 
    phone: 0474 25 82 85
    googlePlaceId: ChIJ2QcI-xABwUcRRYN4VsnZIQA
    latitude: 
    longitude: 
---
Il a travaillé dans l’entreprise familiale, avant d’affiner son expérience dans une distillerie germanophone, pendant 15 ans, comme responsable des développements, tout en gardant un pied dans la fabrication. Il a ensuite créé sa propre liquoristerie en 2012, et propose depuis différentes boissons alcoolisées (les crèmes « M.Intense », les apéritifs « Frui’T » à base de fruits frais, et les liqueurs « M.PEK »).

En 2017, il a élaboré le premier Peket bio belge (« M.PEK 35 »), et plus récemment deux Gin bio (« Mi Gin Bio » et « Mi Gin Spicy Bio »). Michel Bouillon est également spécialisé dans les fabrications à façon.
