---
title: "L'Harmony des Jardins"
date: 2020-08-12T11:20:05+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Education,Ateliers]
summary: >
    Coaching et formations sur le jardinage au naturel et les bienfaits des plantes.
youtubeId: 
valheureux: false
contact: 
    website: http://www.lharmonydesjardins.be
    mail: lharmonydesjardins@gmail.com
    facebook: harmonydesjardins
    twitter: 
    instagram: 
locations:
  - name: L'Harmony des Jardins
    address: Rue Lambert Daxhelet 12<br> 4210 Burdinne/Marneffe
    hours: 
    phone: 
    googlePlaceId: ChIJX0zHhxYIwUcR3EBITCxgqaA
    latitude: 
    longitude: 
---
L'Harmony des jardins vous propose de faire germer votre âme de jardinier avec des formations et un suivi personnalisé.

Avec **l'Harmony des jardins**, vous apprenez comment jardiner avec la nature et pas contre elle. Vous apprenez comment profiter de ce qu'elle a à nous offrir via des ateliers thématiques sur le jardinage au naturel proposés chaque semaine à Marneffe, des ateliers sur les bienfaits des plantes durant la belle saison et du coaching à domicile.
