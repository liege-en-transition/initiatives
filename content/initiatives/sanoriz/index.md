---
title: "Sanoriz"
date: 2020-07-30T10:10:26+02:00
draft: false
longText: false
categories: [Alimentation]
tags: [Produits laitiers, Elevage,Boissons,En ligne,Commerce équitable,Cosmétiques,Produits ménagers,Economie locale,Ateliers]
summary: >
    Sanoriz est une épicerie bio qui travaille avec des produits Belges et si possible locaux. Il y a un rayon de produits frais (fromages, yaourt, beurre,...) mais également des produits cosmétiques, des produits d'entretiens, des compléments alimentaires, des huiles essentielles, fleurs de Bach,... Ils ont tout ce qu'il faut pour le panier de la ménagère. Cette petite boutique a plus de 35 ans et leurs points forts sont le conseil et l'écoute. 

youtubeId: 
valheureux: false
contact: 
    website: https://www.sanoriz.be
    mail: sanoriz@yahoo.fr
    facebook: sanoriz
    twitter: 
    instagram: sanoriz
locations:
  - name: Sanoriz
    address: Rue Servais 17<br>4900 Spa
    hours: 
    phone: 
    googlePlaceId: ChIJQ8tORStiwEcRSHxvXKgt6Jo
    latitude: 
    longitude: 
---
