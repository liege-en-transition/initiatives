---
title: "Ressourcerie et R shop city"
date: 2020-04-29T10:36:03+02:00
categories: ["zero-dechets"]
tags: [Coopérative,Seconde main,Réparation]
summary: >
    La Ressourcerie du Pays de Liège est une société coopérative spécialisée dans la collecte, le tri, le recyclage et la 
    réutilisation des encombrants. Depuis 2010, elle met son savoir-faire à votre service.
youtubeId: ""
contact: 
    website: "https://ressourcerieliege.be"
    mail: "info@ressourcerieliege.be"
    facebook: "RSHOPCity"
    twitter: ""
    instagram: ""
locations:
  - name: "R Shop City"
    address: "Quai St Léonard 79<br> 4000 Liège"
    hours: ""
    phone: "04 220 20 00"
    googlePlaceId: ChIJrzsrApjwwEcRhy6zBT_2J-g
  - name: "R Shop Country"
    address: "Chaussée verte 25/3<br> 4460 Grâce-Hollogne"
    hours: ""
    phone: "04 220 20 00"
    googlePlaceId: ChIJnWhwTub7wEcRncUld5wlLeM
    
---
Sur simple appel, elle reprend à domicile vos encombrants tels que meubles, électroménagers , jouets, bibelots, vaisselle, outils, métaux, plastiques.
Ensuite, elle les remet dans le circuits via deux magasins:
MAGASIN RSHOP COUNTRY à Grace-Hollogne: Ce magasin propose du mobilier de seconde main, des électros à petits prix, 1 an de garantie sur les électros, des articles de puériculture, des livres, etc …
MAGASIN RSHOP CITY à Liège, Quai Saint Léonard: Ce magasin de seconde main vous propose à petits prix des meubles de seconde main, des électroménagers (1 an de garantie), des bibelots, de la vaisselle, des livres, des jouets, des disques, des articles de puériculture et bien plus !

La société SOFIE, qui reconditionne les électros vendus dans les magasins de seconde main RSHOP Country et RSHOP City assure également un service de réparation d’électroménagers en panne pour les particuliers. 
Le recours à des pièces d’occasion permettent de réparer au moindre prix ! Pour le service de réparation, appelez dès aujourd’hui le 04/222 41 11.
