---
title: "Nos Racines"
date: 2020-04-29T09:29:19+02:00
categories: ["Alimentation"]
tags: [Circuit court,En ligne,Vrac,Ateliers,Commerce équitable,Produits ménagers]
summary: > 
    Le projet Nos Racines de l’asbl de Bouche à Oreille est né il y a plus de 30 ans sous le nom de Li Cramignon, en partant du souhait d’un groupe de citoyens de soutenir l’agriculture locale et de consommer des aliments de qualité.
youtubeId: ""
contact: 
    website: "http://www.nosracines.be"
    mail: "circuitscourts@dbao.be"
    facebook: "nosracines.herve"
    twitter: ""
    instagram: ""
locations:
  - name: ""
    address: "Rue Gustave Taillard 31<br>Herve"
    hours: ""
    phone: "087 84 01 46"
    latitude: 50.642045
    longitude: 5.792947
    googlePlaceId: ChIJG7EKLsqMwEcR2E8C85VhIJ8
---
Depuis le départ, la volonté était de réduire l’empreinte écologique, de minimiser les trajets, de réduire les emballages, de favoriser le bio et la récup, d’organiser des ateliers pour apprendre à faire soi-même et de sensibiliser un maximum de consommateurs à ces causes. 

Aujourd’hui, **Nos Racines** est devenu une plateforme de distribution avec e-shop qui livre des commandes à différents points de relais dans la région verviétoise. Vous pouvez également faire vos courses au magasin Oxfam-Nos Racines à Herve (Rue Gustave Taillard n°31) qui vend, en plus des produits issus du commerce équitable, des produits locaux et/ou bio. Vous y trouverez également une zone vrac, des articles de ménage et de bureau pour minimiser vos déchets ainsi que des jeux de société coopératifs.
