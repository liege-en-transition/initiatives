---
title: "Silex Librairie"
date: 2020-07-08T10:07:33+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Seconde main,Solidarité,Sensibilisation,Education,Culture,Ateliers]
summary: >
    Silex est une librairie thématique proposant des ouvrages pratiques, sous forme de manuels, guides didactiques et illustrés sur des sujets tels que la nature, les artisanats, les métiers manuels, le bricolage, la cuisine, le jardinage, la vie pratique, les médecines douces et les remèdes, les loisirs en général, les loisirs créatifs, l’écologie, l’éducation artistique, les DIY, l’électronique et l’informatique, les pédagogies alternatives, le “parenting”, etc.
youtubeId: 
valheureux: false
contact: 
    website: 
    mail: 
    facebook: librairiesilex
    twitter: 
    instagram: librairiesilex
locations:
  - name: Silex
    address: Rue du Pont 34<br> 4000 Liège
    hours: 
    phone: 0497 93 07 14
    googlePlaceId: ChIJL8SuNvX7wEcRa91k61PMj8M
    latitude: 
    longitude: 
---
 Le service y est attentionné, dans un cadre propice à la discussion, à la lecture, et dans une démarche responsable vis-à-vis des méthodes de production du livre. A ce titre, on y trouve aussi des livres d’occasion en tout genre.

Le silex est l’une des roches dans lesquelles nos ancêtres ont façonné leurs premiers outils. Par métonymie, « silex » désigne aussi souvent l’outil lui-même. Ce mot évoque à la fois le geste de l’être humain qui s’approprie son environnement, sa créativité, et la nature sauvage dans laquelle il baignait alors.

Dans cette librairie, vous trouverez des livres sur les tags suivants: Maraîchage, Elevage, Produits laitiers, Boissons ,Farine, Boulangerie, Epices, Produits ménagers ,Cosmétiques, Vêtements, Vegan, Auto cueillette, Réparation, Mobilité douce, Informatique, Apiculture, Pépinière, Fleurs;
