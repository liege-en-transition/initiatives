---
title: "Endless - Selected second hand"
date: 2020-07-08T10:07:53+02:00
draft: false
longText: false
categories: [zero-dechets]
tags: [Vêtements, Seconde main]
summary: >
    Endless, c’est une boutique « chic & cozy » qui propose du prêt-à-porter féminin de seconde main. C’est un lieu aménagé avec goût grâce aux meubles et objets chinés, qui vous invite à dénicher la pièce de vos rêves tout en consommant moins et mieux. Vous y trouverez des vêtements et accessoires tendance et de qualité, qui ont été minutieusement sélectionnés, par le biais d’un système de dépôt-vente. Chez Endless, on reprend le temps d’avoir un véritable coup de cœur, on se tourne vers la « slow-fashion », en donnant une deuxième vie à nos dressings. 
youtubeId: 
valheureux: false
contact: 
    website: 
    mail: endless.secondhand@gmail.com
    facebook: Endless.selectedsecondhand
    twitter: 
    instagram: endless_liege
locations:
  - name: Endless - Selected second hand
    address: Chéravoie 6<br> 4000 Liège
    hours: 
    phone: 0473 11 19 39
    googlePlaceId:  ChIJ-6Mb_VL7wEcRgUSTfoG5h1E
    latitude: 
    longitude: 
---

*Crédits photos: 1, 4 et 5: Pierre Lemoine, 3: Aurore Lefèvre*