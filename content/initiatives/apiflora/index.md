---
title: "Apiflora"
date: 2020-07-08T10:10:47+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Ateliers, Services,Pépinière,Vente directe]
summary: ApiFlora est une pépinière spécialisée dans la production de plantes vivaces indigènes.  Sa particularité est que toutes les plantes mises en culture sont issues de semis ou de souches prélevées localement; les plantes ainsi cultivées sont de ce fait les mieux adaptées à notre environnement.

youtubeId: 
valheureux: true
contact: 
    website: https://www.apiflora.net
    mail: apiflora.be@gmail.com
    facebook: ApiFlora.Belgique
    twitter: 
    instagram: 
locations:
  - name: ApiFlora
    address: Rue Wawehaye 13<br> 4500 Huy
    hours: 
    phone: 0486 50 67 12 
    googlePlaceId: ChIJB9Zsrw2rwUcRZ7SQZiig8XM
    latitude: 
    longitude: 
---
En outre, ApiFlora vous guide dans votre choix de plantes indigènes et vous fait découvrir les éléments-clé pour un jardinage durable. Des ateliers et formations, à l'attention des professionnels et des particuliers, sont organisés tout au long de l'année.
Vous pouvez également faire appel à leurs services pour un aménagement complet ou en partie de vos espaces verts. La formule est souple et peut être accompagnée d'un service de compagnonnage/coaching pour vous rendre autonome dans la gestion et la pérennnisation de vos plantations.

N'hésitez pas à prendre contact pour un aménagement de jardin ou la mise en place d'un système de compagnonnage/coaching
La notion de durabilité fait référence à 3 aspects: l'environnement, l'économie et le social. 

Par ses produits et services proposés, ApiFlora vous accompagne dans la création d'espaces verts durables.
ApiFlora, pépinière en plein développement, souhaite rayonner et participe à un certain nombre d'événements, tels que marchés de produits locaux, foires de jardin et animations diverses.
