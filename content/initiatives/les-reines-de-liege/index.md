---
title: "Les Reines de Liège"
date: 2022-03-01T19:31:04+02:00
draft: false
longText: false
categories: [a_producteur]
tags: [Maraîchage,Circuit court,Commerce équitable,Monnaie locale,Solidarité,Sensibilisation,Ateliers,Apiculture,Pépinière,Fleurs,Ecopaturage]
summary: 
youtubeId: 
valheureux: false
contact: 
    website: 
    mail: "lesreinesdeliege@gmail.com"
    facebook: "lesreinesdeliege"
    twitter: 
    instagram: "les_reines_de_liege"
locations:
  - name: "Les Reines de Liège"
    address: "Boulevard Hector Denis (en face du numéro 179)<br>4000 Liège"
    hours: 
    phone: 
    googlePlaceId: ChIJMcTlSOTxwEcRnfDeuXxzUyg
    latitude: 
    longitude: 
---
Les Reines de Liège, pépinière-rucher, a pour objectif de répondre au déclin des insectes pollinisateurs et aux effets du changement climatique par la production, la vente et la distribution gratuite de plantes mellifères et indigènes, certaines comestibles. Cette ASBL réalise également  des actions de sensibilisations et animations.

