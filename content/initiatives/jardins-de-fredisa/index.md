---
title: "Jardins De Fredisa"
date: 2020-07-08T10:04:34+02:00
draft: false
longText: true
categories: [a_producteur]
tags: [Maraîchage,Elevage,Epices,Produits ménagers,Cosmétiques,Vente directe,Circuit court,Apiculture]
summary: >
   Depuis le printemps 2017, Isabelle Mathieu et son mari Frédéric Heneaux ont créé Les Jardins de Fredisa où se pratiquent le maraîchage, l’apiculture et le petit élevage dans le respect de la nature et de la santé. Tous deux dans l’enseignement, sans quitter totalement leur emploi, ils ont sauté le pas “de la Culture à l’agriculture”!
youtubeId: 
valheureux: true
contact: 
    website: 
    mail: mathieu-isabelle@hotmail.com
    facebook: Jardinsdefredisa
    twitter: 
    instagram: 
locations:
  - name: Les Jardins de Fredisa
    address: Chemin d'Insegotte 10<br> 4181 Hamoir
    hours: 
    phone: 0479 51 93 59
    googlePlaceId: ChIJbzkBz15awEcRiprKKrI8WDw
    latitude: 
    longitude: 
---
Ils ont transformé leur belle pelouse en plusieurs potagers et y ont érigé serres et tunnels. On y voit chèvres, poules, canards, cailles, lapins.

Toute l’année, ils préparent des paniers de légumes à emporter ou servis à domicile. Le magasin est ouvert le samedi de 10 à 14h de mai à octobre inclus. On y trouve des légumes de saison, du miel de leurs ruches, des œufs, des jus de pommes, des confitures, des betteraves et cornichons en bocaux, la bière régionale «La Ferrusienne», des petites liqueurs. On peut aussi s’y fournir en cosmétiques et produits d’entretien maison. 

En plus, ils proposent aussi la création et l’entretien de pelouses et le placement de châssis.
