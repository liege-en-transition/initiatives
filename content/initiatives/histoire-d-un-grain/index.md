---
title: "Histoire d'un Grain"
date: 2021-08-20T17:45:23+02:00
draft: false
longText: false
categories: [a_producteur]
tags: [farines,coopérative]
summary: "Coopérative agricole et meunière sur le Plateau de Herve (production de farines certifiées Bio, Agriculture locale, Mouture meule de pierre)"
youtubeId: 
valheureux: false
contact: 
    website: "http://www.histoiredungrain.be"
    mail: "bonjour@histoiredungrain.be"
    facebook: "histoiredungrain"
    twitter: 
    instagram: 
locations:
  - name: Histoire d'un Grain
    address: "Rue de la chapelle, 36<br>4630 Soumagne"
    hours: 
    phone: 
    googlePlaceId: ChIJ21JPz1KMwEcRYPyxOypLRHU 
    latitude: 
    longitude: 
---
**Histoire d’un grain** est une jeune coopérative agricole et meunière engagée dans la réinsertion de céréales de qualité pour l’alimentation locale humaine. Créée officiellement le 25 avril 2018, elle a pour objet la **production et transformation de céréales et autres cultures associées sur le Pays de Herve**.
