---
title: "Oh! Bio & Terroir"
date: 2020-04-29T11:35:03+02:00
categories: ["Alimentation"]
tags: [Circuit court,Restauration,Cosmétiques,Produits ménagers,Education,Ateliers,Sensibilisation,Evènement]
summary: "Oh ! Bio & Terroir, c’est un magasin de proximité à Herve proposant des produits alimentaires issus de l’agriculture biologique, locaux de préférence, ainsi que des produits pour la maison, les soins du corps et des produits naturels pour la santé et le bien-être : compléments alimentaires, plantes, huiles essentielles."
youtubeId: ""
valheureux: true
contact: 
    website: "http://oh-bio-et-terroir.be"
    mail: "info@oh-bio-et-terroir.be"
    facebook: Oh-Bio-Terroir-947415962004633
    twitter: ""
    instagram: ""
locations:
  - name: ""
    address: "Outre-Cour 45-89<br>4651 Herve"
    hours: "Du lundi au samedi <br> De 9h à 17h"
    phone: "0473 93 54 33"
    latitude: 50.637731
    longitude: 5.769896
    googlePlaceId: ChIJX98ENkbzwEcRLK__LFZeQMY
---

### Oh ! Resto
Petite restauration saine & bio
Un concept original, des lunchs sains et colorés préparés avec soin et une équipe dynamique qui vous reçoit du mardi au vendredi de 12h à 14h

### Oh ! Équilibre
Espace de développement personnel & créatif
Une équipe de praticiens ainsi que des ateliers, conférences et formations pour développer votre savoir corporel, émotionnel et créatif.
