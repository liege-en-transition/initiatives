---
title: "Eco Loop"
date: 2020-07-08T10:03:29+02:00
draft: false
longText: true
categories: [zero-dechets]
tags: [Sensibilisation,Ateliers,Puericulture,Produits ménagers, Mobilité douce]
summary: >
    Eco-Loop, c’est la possibilité pour les (futurs) parents de découvrir les couches lavables via des ateliers, de les essayer avec des kits de location & de s’alléger l’éventuel travail additionnel par leur service de ramassage (à vélo)
youtubeId: 
valheureux: false
contact: 
    website: 
    mail: eco-loop@outlook.com
    facebook: 
    twitter: 
    instagram: 
locations:
  - name: Eco-Loop
    address: Rue Vinâve 11<br> 4260 Fallais 
    hours: 
    phone: 0486 83 38 18
    googlePlaceId: ChIJhwcQHNEHwUcR7bfGMsjEtB4
    latitude: 
    longitude: 
---
La santé de votre bébé est une priorité?
L'environnement dans lequel il s'épanouira aussi?
Les couches lavables, ça vous parle... un peu, beaucoup, ou pas du tout encore?
Nathalie organise des ateliers découvertes une fois/mois ou sur demande!
Elle vous propose des packs de locations afin de trouver LE système qui vous convient à vous et à votre bébé (et à la gardienne, la crèche, les grands-parents...)

**Vous êtes convaincus mais vous avez peur de l'éventuelle charge de travail associée?** Elle propose également un service de nettoyage pour un mois à ... quand vous voulez!
En option, le ramassage des couches sales et dépôt de couches propres... à l’énergie de ses gambettes!
