---
title: "Un Pain C'est Tout"
date: 2021-08-20T18:07:08+02:00
draft: false
longText: false
categories: ["a_producteur"]
tags: [Farine,Boulangerie,Circuit court,Sensibilisation,Education]
summary: "La boulangerie vous propose des pains blancs ou complets, croissants, miches, gosettes, demi-tartes aux fruits de saison, craquelins d'épeautre, ..."
youtubeId: 
valheureux: false
contact: 
    website: "https://sanspatron.be/"
    mail: "info@sanspatron.be"
    facebook: "unpainctout"
    twitter: 
    instagram: 
locations:
  - name: "Un Pain C'est Tout"
    address: "Rue de la Loi 20<br>4020 Liège"
    hours: 
    phone: 
    googlePlaceId: ChIJR04BEafwwEcRKvx-YwRRHpg
---

**"Faire du pain, c’est penser à la qualité  et à la provenance de la farine, au lieu et la façon dont ce pain sera produit, à l’économie d’énergie, au rapport du producteur au consommateur, à la santé, à la mobilité et à la gestion du surplus. Il s’agit de favoriser une alimentation saine et durable, produite de façon saine et durable".**

Leur pain est produit à base de farine de froment et d’épeautre en provenance exclusive du Moulin de Hollange et sans aucun ajout d'agent technologique, ce qui lui confère une qualité nutritionnelle supérieure et permet d’obtenir un goût unique tout en gardant sa texture et sa couleur authentique.

En s'intégrant dans une démarche filière courte, le boulanger devient le lien direct entre le producteur et le consommateur; le garant de la qualité des produits.

Situés au 20 rue de la Loi, dans un quartier d’Outremeuse où vivent étudiants, jeunes ménages et personnes âgées, la boulangerie et son atelier préservent le calme de cette charmante place et ont su recréer une cohabitation entre activités artisanales et habitat.
