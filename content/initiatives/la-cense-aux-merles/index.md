---
title: "La Cense Aux Merles"
date: 2020-07-08T10:06:29+02:00
draft: false
longText: false
categories: [a_producteur]
tags: [Maraîchage,Elevage,Auto cueillette,Vente directe,Circuit court,Apiculture,Pépinière,Fleurs]
summary: >
    La Cense aux Merles est une micro-ferme en permaculture dans la région de Hannut/ Huy/ Waremme.
    Ils cultivent un verger à Wasseiges et un verger à Braives selon les principes de la permaculture.
    Un financement participatif a permis de financer l'élevage de poules et de moutons sur leurs parcelles.
youtubeId: 
valheureux: false
contact: 
    website: http://www.lacenseauxmerles.be
    mail: lacenseauxmerles@gmail.com
    facebook: censeauxmerles
    twitter: 
    instagram: 
locations:
  - name: La Cense aux merles
    address: Les Folihottes<br> 4260 Braives
    hours: 
    phone: 0474/894864
    googlePlaceId: ChIJ2V_wJjkHwUcRJmmJewSHtkQ
    latitude: 
    longitude: 
---
