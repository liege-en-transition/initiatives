---
title: "Mangez Local"
date: 2020-07-08T10:08:55+02:00
draft: false
longText: true
categories: [zz-autres]
tags: [Sensibilisation,Circuit-court,En ligne]
summary: >
    La mission de Mangez Local ! est de soutenir et de faire connaître les producteurs artisanaux et de les mettre en lien avec les consommateurs locaux.
youtubeId: MYAPj2Z2Xuw
valheureux: false
contact: 
    website: https://www.mangez-local.be/fr
    mail:  info@mangez-local.be 
    facebook: MangezLocal
    twitter: 
    instagram: 
locations:
  - name: 
    address: 
    hours: 
    phone: 0496/642650
    googlePlaceId: 
    latitude: 
    longitude: 
---
En achetant des produits authentiques, naturels et peu transformés, les consommateurs bénéficient d’une alimentation de haute qualité nutritive, d’un goût incomparable et de la garantie de l’origine des produits. C’est tout bénéfice pour la santé et le plaisir de la table.

En consommant des produits de son terroir, le consommateur favorise l’économie locale et contribue ainsi à une juste rémunération des artisans et des producteurs de façon durable.
En favorisant le circuit court, les producteurs et les consommateurs réduisent ensemble l’impact de la pollution émise par les transports au profit de la santé publique et de la biodiversité.
La rencontre entre les artisans, les producteurs et les consommateurs, favorise le lien social et les relations authentiques entre les personnes.

En plus de cette plateforme favorisant le lien entre producteurs et consommateurs, Mangez Local! met en évidence dans des capsules vidéos le savoir faire de nos producteurs et artisans belges.
Retrouvez les sur la chaine YouTube: https://www.youtube.com/channel/UCY5vKWsreIOqlBKrjks4rGQ

