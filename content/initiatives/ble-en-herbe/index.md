---
title: "Le Blé en Herbe"
date: 2020-07-08T10:03:21+02:00
draft: false
longText: true
categories: [Alimentation]
tags: [Produits laitiers,Boissons,Farine,Boulangerie,Produits ménagers, Circuit Court, Vrac]
summary: Installé depuis 1985 dans le centre de Tilff, le blé en herbe est une des épiceries pionnières en alimentation biologique dans la région liégeoise.
youtubeId: 
valheureux: false
contact: 
    website: http://www.bleenherbe.be
    mail: infos@bleenherbe.be
    facebook: Le-blé-en-herbe-Epicerie-naturelle-Tilff-215462811840504 
    twitter: 
    instagram: 
locations:
  - name: Le blé en herbe (Epicerie naturelle - Tilff)
    address: Avenue Laboulle 90<br> 4130 Esneux
    hours: 
    phone: 04 388 15 16 
    googlePlaceId: ChIJKY8OTo33wEcRrTitwizD-qk
    latitude: 
    longitude: 
---
Avec presque 35 années d'expérience, cette entreprise familiale a pu se spécialiser tout en restant fidèle à ses valeurs de départ. L'équipe est composée de 9 personnes expérimentées (en herboristerie, diététique, aromathérapie...) et est toujours à votre disposition pour vous conseiller au mieux.

Depuis cette année, le vrac a fait son grand retour avec un tout nouveau rayon composé de 36 références choisies avec beaucoup de soin (origine, emballage, saveur...).

4 boulangeries artisanales collaborent avec la boutique et y déposent leurs pains frais chaque jour: Benoît Segonds, Le Pont, Le Pain Divin et Les Co'Pains.

**Le Blé en herbe**, c'est l'assurance de faire ses courses dans un lieu où le bio n'est pas juste "tendance", mais un lieu où l'on prône ce qui nous semble bon pour la terre depuis longtemps.
