---
title: "Cuisine Ouverte"
date: 2020-08-12T11:18:52+02:00
draft: false
longText: true
categories: []
tags: []
summary: >
    Cuisine Ouverte c’est …
    … une cuisine ouverte :) une cantine bistronomique
    Où le client voit tout ce qui se passe, où on voit la cheffe aux fourneaux, qui explique avec passion ses produits, ses préparations, ses coups de cœur …

youtubeId: 
valheureux: false
contact: 
    website: http://www.cuisine-ouverte.be/	
    mail: info@cuisine-ouverte.be
    facebook: cuisineouverte
    twitter: 
    instagram: cuisineouverteliege
locations:
  - name: 
    address: Rue du Vertbois 9<br>4000 Liège
    hours: 
    phone: 0498 03 08 83
    googlePlaceId: ChIJ--EYsg77wEcR8MI3vnM16PY
    latitude: 
    longitude: 
---
La cheffe, c’est Emmanuelle Horion, qui a choisi de changer de vie en 2018, de quitter son travail dans les Ressources Humaines, pour exercer sa passion depuis toujours : ravir les papilles de ses convives.

Une cuisine qu’elle veut inventive et respectueuse des produits et des saisons, des assiettes colorées et savoureuses, le tout fait maison.

Une carte de saison propose un lunch 2 ou 3 services ou à la carte, qui change régulièrement, des plats confectionnés avec des produits issus de producteurs locaux, majoritairement bio ( Pain de chez Benoit Segonds, légumes, fruits, fromages de la Coopérative Ardente, viande de la boucherie Schmitz (rue Saint-Gilles), saumon de chez l’Artisan du Saumon à Boirs, café de chez l’Artisan du Café à Verlaine, volaille label Coq des Prés, …)

La cheffe propose également une jolie carte de vins, essentiellement bio, soigneusement sélectionnés et qui varie en fonction de la carte. On y trouve du Vin de Liège, évidemment!

Mais aussi un bel assortiment de bières, issues de micro brasseries liégeoises pour la plupart.

En salle, c’est Joanna qui vous accompagnera avec le sourire, et dans l’ombre Jacqueline s’affaire, pour vous faire passer un agréable moment de détente gustative.

Un cadre agréable à deux pas du centre ville, où on peut passer un temps de midi sympa à deux, à quatre, ou … à 12 autour de la grande table.

Idéal pour un temps de midi entre collègues ou amis, un lunch-réunion de travail, un repas familial avec les enfants, tout est possible!…