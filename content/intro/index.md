---
title: Comment agir dès maintenant pour la Transition?
slug: introduction
resources:
- name: cover
  src: liege-c-tro-bo-2021.png
coverCredits:
  name: mp
  url: 
summary: >
    Votre guide pour la Transition à Liège! Ce site rassemble les initiatives de particuliers ou de coopératives qui permettent à la région liégeoise de s’inscrire dans une démarche de Transition.
---

## Transition vers quoi ?

**Transition** vers une économie circulaire, durable qui ne suit plus l’idéologie de la croissance perpétuelle et de la consommation.

**Transition** vers un respect nécessaire et urgent de la faune et de la flore environnante.

**Transition** vers un milieu social plus riche en collaborations, vers une solidarité plus forte entre liégeois qui nous apportera plus de résilience pour les défis actuels et ceux à venir.

{{< image src="liege.jpg" title="" >}}

## Ces projets sont très nombreux...

**Et notre site est loin d’être complet!** Nous continuerons à les rassembler tout au long de l'année et nous invitons bien sûr les porteurs de projets à nous [contacter directement](mailto:transitionliege@gmail.com). 

Pour le moment notre site et pages servent principalement de relais vers le site et réseaux de chaque initiative, dans l'idée de renforcer leur diffusion, et de faciliter l'accès à chacun vers ces découvertes. 
Toutes vos idées sont plus que les bienvenues pour faire évoluer notre action et améliorer notre site. Parmi celles-ci, nous comptons déjà:
* La recherche par tags
* Enrichir nos pages d’articles, interviews, vidéos
* L’ajout d’une carte interactive et de circuits de découverte
* Calendrier des événements et marchés locaux

Notre mouvement est avant tout citoyen donc n'hésitez pas à nous contacter et apporter des réflexions et projets pour dynamiser la Transition à Liège!

{{< image src="terril2.jpg" title="" >}}

## Ecologie et partage des outils

Ce site a été élaboré de manière la moins énergivore possible:
* Choix délibéré d’un thème simple sans fonctionnalités inutiles
* Pages générées à l’avance et rendues de manière statique
* Utilisation de polices système
* Optimisation des images et des scripts

Il est également [open source](https://gitlab.com/liege-en-transition/initiatives) afin que n’importe qui puisse se l’approprier, et pourquoi pas répliquer l’initiative dans sa propre ville: le mouvement de la Transition est bien international dans le partage des expériences, de la philosophie et des outils, en imaginant ainsi un *monde-village* plus local mais tout autant connecté. 

Pour en savoir plus:
* [Réseau transition (Belgique)](https://www.reseautransition.be/)
* [Transition Network (International)](https://transitionnetwork.org/) *En Anglais*

<br>

## Par où commencer? 

Voici quelques liens utiles qui donnent un bel aperçu des projets qui favorisent la Transition à Liège: 

* [Ceinture Alimen-terre Liégeoise (CATL)](https://www.catl.be/) *Réseau principal pour la transition alimentaire à Liège*
* [Val'heureux](https://valheureux.be) *Monnaie locale liégeoise: où s'en procurer, où les utiliser* 
* [Permis de végétaliser](http://www.permisdevegetaliser.be/) *Chacun peut rendre la ville plus verte et plus solidaire!* 


Nos pages **Instagram** et **Facebook** (liens en haut à droite de la page) serviront également de relai pour vous informer de tout ce qui se passe dans la région, ainsi que pour diffuser nos actions et celles des nombreux projets existants, pour ne rien louper! 

Enfin, voici les formulaires qui permettent de nous signaler une erreur ou de nous partager un projet: 

[→ Ajouter un projet au site](https://forms.gle/aK9C8uPXudokhfUP7)
[→ Signaler une erreur](https://forms.gle/zpG2mFREkMSw48jH6)


Nous vous souhaitons de bons partages et, nous l’espérons, de bonnes découvertes.


*L’équipe du collectif ‘Liège en Transition’.*
