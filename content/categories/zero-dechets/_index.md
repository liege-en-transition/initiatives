---
title: "Zéro Déchets"
subtitle: 
colorId: 5
summary: >
    A l’aide de gestes faciles à adopter au quotidien nous pouvons réduire autant que possible le plastique et tout ce qui est jetable. C’est donc ici que vous trouverez les initiatives qui proposent du vrac et de la récup, alimentaire ou non!   
--- 
**Projets en cours de contact:** Kalbut Design, Made and more, Maison des Plantes, Make your cup. [Vous en connaissez d'autres?](https://forms.gle/aK9C8uPXudokhfUP7)
