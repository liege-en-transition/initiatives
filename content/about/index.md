---
title: "Qui sommes-nous ?"
slug: a-propos
menu: "main"
linkTitle: En savoir plus sur Liege en Transition
summary: Liège en transition est une action de communication qui s'inscrit dans le mouvement de la Transition, en région liégeoise.
---

## La Transition

Le mouvement de la Transition est né en 2006 sous l’impulsion d’un groupe de citoyens initié par Rob Hopkins qui pose un constat clair: “nous devons agir rapidement, sans attendre que les gouvernements parviennent enfin à s’entendre et à passer à l’action”. Depuis lors, ce mouvement s’est largement répandu et de nombreuses initiatives ont vu le jour! Alors avec quelques copains, on s’est demandé ce qui se passait sur notre territoire liégeois...

## Liège en Transition

Notre initiative part de 2 constats principaux:

* Des initiatives de transition existent déjà sur le territoire liégeois: des projets se créent et des citoyens se réapproprient l’économie, suscitent l’esprit d’entreprise, réimaginent le travail, développent de nouvelles compétences et tissent des réseaux de liens et de soutien. 
* De nombreux citoyens liégeois nous on fait part d’un manque d’informations sur le sujet. Beaucoup d’entre eux ne semblent pas au courant de ce qu’il existe près de chez eux. Or, ils sont nombreux à manifester un intérêt pour la transition et aimeraient faire le pas vers ces initiatives. 


Pour répondre à ces constats, nous développons depuis 2019 une base de données et un site web qui recense les initiatives de transition déjà existantes sur le territoire liégeois. Par nos actions de communication, nous souhaitons rendre ces initiatives locales et citoyennes plus visibles. Notre groupe se veut ouvert, bénévole - sans aucune intention lucrative ou politique - et s'inscrit dans une philosophie open-source: partager nos outils, méthodes et expériences pour que d'autres régions puissent les utiliser à leur tour.


Si vous voulez en savoir plus envoyez-nous un message, on y répondra au plus vite! 

## Equipe et contact 

Notre équipe est principalement composée de 8 membres: Lionel Brackman, Céline Maréchal, Corentin Lenelle, Marie-Pierre Damas, Martin Pierlot, Jonathan Berger, Mathilde Withaeghs, Mona Struman, Claudia Franckh, Michaël Verbeeck, aidés par les membres de notre groupe Facebook (environ 90). 

Nous sommes en train d'organiser des groupes de travail (communication, base de données) afin d'inclure un maximum d'intéressés à nos actions et ainsi les développer ensemble. 

* Email: transitionliege@gmail.com
* Vous pouvez aussi nous contacter directement via message sur notre page Facebook, ou Instagram.

## Données personnelles

*Dans l'attente d'une meilleure option, nous utilisons Google Analytics pour une analyse globale et anonyme du trafic du site Web, qui est importante pour savoir commment diriger nos efforts. Afin de suivre votre utilisation de session, Google dépose un cookie (_ga) avec un ClientID généré aléatoirement dans votre navigateur. Cet identifiant est anonyme et ne contient aucune information identifiable telle que le courriel, numéro de téléphone, nom, etc. Nous envoyons également votre adresse IP à Google. Nous utilisons GA pour suivre le comportement global du site Web, comme les pages que vous avez consultées, pendant combien de temps, etc.*

*Si vous souhaitez accéder aux informations de navigation dont nous disposons - ou nous demander de supprimer des données GA - veuillez nous contacter, supprimer vos cookies _ga, ou installer [cette extension](https://chrome.google.com/webstore/detail/google-analytics-opt-out/fllaojicojecljbmefodhfapmkghcbnh)*
