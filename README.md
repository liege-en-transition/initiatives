# Liege en Transition: Initiatives

## What is this ?

_Liege en Transition: Initiatives_ is a platform that
references places that support a more sustainable
way of living.

Places (_Initiatives_) are organized in categories (food, crafts, recycling,...) and 
display pictures, a description, a map and contact details.

Featured stories about places will be added gradually and will 
display a video targeted at social sharing as well as interviews and photos.  

## Under the hood

This website is created with [Hugo](https://gohugo.io/). 
Hugo is a static site generator, which means that all content 
is compiled into HTML files and then served to the browsers. 
No need for databases and server-side languages like PHP or Node.js.

The obvious benefit is performance: server doesn't need a lot of
CPU and memory to send the files, and browsers will display the
pages faster.

## How to run

Adding content is 3 step process:

1. Start a local Hugo server to run a local version of the website.
1. Change markdown and resource files in content directory.
1. Use git to commit changes and push them to Gitlab.

This requires that you have Git and Hugo installed on your computer
and that you have checked out this repository.

### Install Hugo

See https://gohugo.io/getting-started/installing/

### Clone repository

Depending on your setup there are multiple ways you can install git. 

For Windows users, [Git for Windows](https://gitforwindows.org/) might be
a good place to start. It provides a GUI to manipulate git repositories
but it's pretty basic so you might want to find another software. 

Another solution is to install [Visual Studio Code](https://code.visualstudio.com/). It's
also free and will also let you edit the markdown easily files when you'll
manage content. 

[Github Desktop](https://desktop.github.com/) is a free software 
with all the features needed and will work with Gitlab (see [here](https://itnext.io/how-to-use-github-desktop-with-gitlab-cd4d2de3d104));

### Run local server

Go to the directory where you cloned this repository and run:

`hugo server -D --buildDrafts`

Then open your browser at `http://localhost:1313`.

Every time you make changes in the `content/` directory
the website will automatically update.

## Content management

In Hugo, content is written in Markdown. Everything is stored 
in the `content/` directory.

Organization is [covered extensively in the docs](https://gohugo.io/content-management/organization/)
 but for our purpose it should be pretty straightforward. 

Each page has metadata associated with it (Hugo calls that "Front Matter"). 
They're added in the beginning of the Markdown file between two `---` lines and
written in YAML, TOML or JSON format (default for this project is YAML).

More info in the docs: https://gohugo.io/content-management/front-matter/ 

### Initiatives

Initiatives are stored in the `content/initiatives/` folder. 

Each initiative must have at least :

* Its own directory
* A `index.md` file with all the content and metadata
* A `cover.jpg|png` image that will be displayed on home and category pages

You can generate an empty initiative directory with :

`hugo new --kind initiative-bundle initiatives/my-new-initiative`

(replace `my-new-initiative` by the title of your initiative).

#### Front Matter (metadata)

##### title

The title of the initiatives. Will be displayed everywhere. 

A default title is generated from the `hugo new` command explained earlier,
but feel free to change it.

##### date

Date of creation. Will be used as publish date on the initiative page,
can be changed.

##### draft

Set this to true if you want to hide this initiative. That can 
be useful if you're working on something that you don't want published yet. 

##### longText

Setting this to true will display a slightly different layout (summary 
and tags will be shown right beneath title instead of in content block).

##### categories

The category this initiative belongs to. An initiative can belong to
multiple categories but avoid that as much as possible.

##### tags

Some keywords associated with the initiative. Not used right now (apart from
 being displayed on an initiative page) but could be useful in the 
 future (for a search engine or an associated content feature).

##### summary

Summary is displayed as the first paragraph right beneath the title
on an initiative page and also on the featured block on the homepage. 
It should be short (one or two sentences) and catchy.

##### youtubeId

The ID of the Youtube video that will be shown on the initiative page.
If there's no ID the cover will be shown instead.

You can get the ID by looking at the URL:

> youtube.com/watch?v=**e9XWt2PFhwI**

##### contact (website, mail, facebook, twitter, instagram)

Contact info for the initiative. They are all optional.

Only write relevant part, not the entire URL.
 
Example: write `auvertg` instead of `https://www.facebook.com/auvertg/`  

##### locations

A location has different informations: Name, Address, Hours, Phone.

An initiative can have one or multiple locations. Here's how you would
write the metadata for two locations:

```yaml
locations:
  - name: Lieu 1 # Useful when there are multiple locations
    address: Rue de la ville, 4000 Liège
    hours: > # lets us write on multiple lines
      mardi: 15:00 - 19:00 <br>
      vendredi: 10:00 - 19:00
    phone: 04987123456
    googlePlaceId: # will be used in priority over latlong  if defined
    latitude: # Use only if place is not on GMaps
    longitude: 
  - name: Lieu 2
    ...
```

Google Place ID is better than coordinates since it will directly display a sidebar with info on the place
and put the pin exactly where it should be. When defined it will take precedence over coordinates. **Use
coordinates as a fallback when the place cannot be found on Google Maps**.

Use this page to search for a Google place ID: https://developers.google.com/places/place-id

Also note the chevron (`>`) and `<br>` tag for the hours: it lets us write text on multiple lines.

#### Featured

A featured initiative is displayed on homepage. You can change it in `content/_index.md`.

Note that the value must match the folder name of the initiative.

### Categories

Adding a category is easy: you just have to add it to an initiative
metadata. But categories should have a subtitle and a color. To add them,
you have to create a markdown file in `content/categories/`.

The category should have a `colorId` key associated to a color defined
in the theme (`themes/let/assets/_theme.scss`).

You can generate a category with :

`hugo new --kind category categories/my-new-category`.

#### Title

Title will be generated automatically based on the folder name.
Feel free to change it.

#### Subtitle

Subtitle will be shown on the homepage alongside the category title.
It should be seen as a sort of baseline that reflects the positive side
of the category.

#### ColorId

Refers to a color defined in the theme: `themes/let/assets/scss/utl/_colors.sss`.

Example: ColorId 1 refers to SCSS variable `$color-1`.

Change colors in theme.scss file.

### Menus

There are 3 different menus in the website: the category menu, the 
main menu and the social menu.

#### The Category Menu

The category menu is generated automatically. For a category to be shown, 
it has to be defined in the metadata of at least one initiative. Creating
it in `content/category/` is not enough. 

#### The Main Menu

The main menu only has the _About_ page for now but could be used for
other static pages in the future. 

To add a page to the menu, add a `menu` key in the metadata of the markdown
file: 

```yaml
menu: "main"
```

#### The Social Menu

The social menu lists all social pages associated with the project.
 
It is defined in `config.yml`:

```yaml
...

menu:
  social:
    - name: Fb
      identifier: facebook
      url: https://facebook.com
    - name: Tw
      identifier: twitter
      url: https://twitter.com
    - name: Ig
      identifier: instagram
      url: https://instagram.com
    - name: Yt
      identifier: youtube
      url: https://youtube.com
...
```

(Identifier should not have any special characters or spaces since it will
be used as a CSS class).

### Static pages

For now there's only the About page, but there could be more static pages
in the future.

To create a static page, either create a markdown file in the content
directory or create a directory with an index file. So both of those
paths will work:

`static-page/index.md` \
`static-page.md`

The first approach is better since it will let us store resources 
(pictures, videos, files,...) in a cleaner way.

To generate a static page, run :

`hugo new --kind static-page my-static-page`

Note: if you don't want the page to appear in the main menu, remove the
metadata key `menu` from the markdown file.

#### About Page

A block of text will be displayed in the footer of every page with a 
call-to-action to the About page.

The text block is generated with the metadata key `summary` in 
the _about_ markdown file. 
