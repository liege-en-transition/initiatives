---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: false
longText: false
categories: []
tags: []
summary: >
    Small summary (1 or 2 sentences max).
youtubeId: 
valheureux: false
contact: 
    website: 
    mail: 
    facebook: 
    twitter: 
    instagram: 
locations:
  - name: 
    address: 
    hours: 
    phone: 
    googlePlaceId: 
    latitude: 
    longitude: 
---

Content in markdown format.

Markdown syntax: https://www.markdownguide.org/basic-syntax